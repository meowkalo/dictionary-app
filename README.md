# Dictionary App
#### Simple iOS App for learning English words

**Provides**
- Multiple categories of words to learn
- Search by words
- Tests of knowledge

#### Screenshots
<p align="center">
    <img src="https://i.ibb.co/tMPgmnT/2020-03-18-12-51-14.png" width="30%" title="1">
    <img src="https://i.ibb.co/s9BfN2s/2020-03-18-12-51-22.png" width="30%" title="2">
    <img src="https://i.ibb.co/WvSyysX/2020-03-18-12-52-19.png" width="30%" title="3">
    <img src="https://i.ibb.co/WvSyysX/2020-03-18-12-52-19.png" width="30%" title="4">
    <img src="https://i.ibb.co/BjVZZhN/2020-03-18-12-52-51.png" width="30%" title="5">
</p>

**Made with Swift 5, Realm DB and contains Api requests (emulation)**
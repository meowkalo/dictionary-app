//
//  CardViewAlt.swift
//  Dictionary
//
//  Created by Алена Алексиади on 10/03/2020.
//  Copyright © 2020 Алена Алексиади. All rights reserved.
//

import UIKit

class CardViewAlt: UIView {
    
    var word1 = UIButton()
    var word2 = UIButton()
    var word3 = UIButton()
    var word4 = UIButton()
    private var stack = UIStackView(frame: CGRect(x: 0, y: 0, width: 278, height: 308))
    
    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: 278, height: 358))
        
        self.layer.cornerRadius = 20
        self.backgroundColor = .white
        self.layer.borderWidth = 3
        
        initButton(button: word1)
        initButton(button: word2)
        initButton(button: word3)
        initButton(button: word4)


        stack.addArrangedSubview(word1)
        stack.addArrangedSubview(word2)
        stack.addArrangedSubview(word3)
        stack.addArrangedSubview(word4)
        stack.axis = NSLayoutConstraint.Axis.vertical
        stack.distribution = UIStackView.Distribution.equalSpacing
        stack.alignment = UIStackView.Alignment.center
        stack.spacing = 15
        stack.center = self.center
        
        self.addSubview(stack)
    }
    
    func change(color: UIColor) {
        self.layer.borderColor = color.cgColor
        self.layer.borderColor = color.cgColor
        self.word1.backgroundColor = color
        self.word1.setTitleColor(.white, for: .normal)
        self.word2.backgroundColor = color
        self.word2.setTitleColor(.white, for: .normal)
        self.word3.backgroundColor = color
        self.word3.setTitleColor(.white, for: .normal)
        self.word4.backgroundColor = color
        self.word4.setTitleColor(.white, for: .normal)
    }
    
    private func initButton(button: UIButton) {
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        button.titleLabel!.textAlignment = .center
        button.layer.cornerRadius = 25
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.widthAnchor.constraint(equalToConstant: 230).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


//
//  CardView.swift
//  Dictionary
//
//  Created by Алена Алексиади on 10/03/2020.
//  Copyright © 2020 Алена Алексиади. All rights reserved.
//

import UIKit

class CardView: UIView {
    
    var word = UILabel(frame: CGRect(x: 0, y: 0, width: 240, height: 240))

    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: 280, height: 360))
        
        self.layer.cornerRadius = 20
        self.backgroundColor = .white
        self.layer.shadowRadius = 15
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = .zero
        
        word.text = ""
        word.textColor = .white
        word.textAlignment = .center
        word.numberOfLines = 0
        word.font = UIFont.systemFont(ofSize: 26, weight: .heavy)
        word.center = self.center
        self.addSubview(word)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

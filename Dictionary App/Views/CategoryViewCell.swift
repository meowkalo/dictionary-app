//
//  CategoryViewCell.swift
//  Dictionary App
//
//  Created by Алёна Алексиади on 01/03/2020.
//  Copyright © 2020 Алёна Алексиади. All rights reserved.
//

import UIKit

class CategoryViewCell: UICollectionViewCell {
    
    static let reuseIdentifier = "CategoryViewCell"

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var image: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 5.0
        self.layer.shadowOpacity = 0.7
        self.layer.masksToBounds = false
        contentView.layer.cornerRadius = 15
        image.backgroundColor = .clear
        image.contentMode = .scaleAspectFit
        image.tintColor = UIColor.white.withAlphaComponent(0.7)
        title.textColor = .white
    }

    func configureCell(with data: Category, color: String) {
        title.text = data.name_ru
        image.image = nil
        image.loadImage(fromURL: data.icon)
        self.contentView.backgroundColor = UIColor.init(hex: color)
    }
}

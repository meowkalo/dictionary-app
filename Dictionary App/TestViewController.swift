//
//  TestViewController.swift
//  Dictionary App
//
//  Created by Алёна Алексиади on 10/03/2020.
//  Copyright © 2020 Алёна Алексиади. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {
    
    //две карточки, сменяющие друг друга
    let Card1 = CardView()
    let Card2 = CardView()
    //карточка с варианатми ответа
    let CardAlt = CardViewAlt()
    //лейбл результатов
    let resultLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 240, height: 100))
    //жест, который будет разпознаваться
    var gesture = UIPanGestureRecognizer()
    //точка, куда нужно отправлять невидимую сейчас карточку
    var lowerCenter = CGPoint()
    
    var category = Category()
    var words = [Word]()
    var correctWord = ""
    var correctCounter = 0
    var bgColors = ["#007EFE", "#00BB00", "#f59f00", "#FA5002", "#B40983"]
    var c = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = category.name_ru
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        c = 0
        words = Array(category.words).shuffled()
        initCards()
    }
    
    func initCards() {
        //вычисляем точку для невидимой карточки
        lowerCenter.x = self.view.center.x
        lowerCenter.y = self.view.frame.height + 180
        
        //создаем жест и связываем его с функцией
        gesture = UIPanGestureRecognizer(target: self, action: #selector(self.wasDragged(gestureRecognizer:)))
        gesture.delegate = self
        
        //устанавливаем жест, позицию и внешний вид для первой карточки
        Card1.center = self.view.center
        Card1.addGestureRecognizer(gesture)
        Card1.backgroundColor = UIColor.init(hex: bgColors[0])
        Card1.word.text = category.words[c].en
        
        //устанавливаем позицию для второй карточки
        Card2.center = lowerCenter
        
        //устанавливаем позицию и функции для кнопой карточки с ответами
        CardAlt.center = self.view.center
        CardAlt.change(color: UIColor.init(hex: bgColors[0])!)
        CardAlt.word1.addTarget(self, action: #selector(self.answerPressed(sender:)), for: .touchUpInside)
        CardAlt.word2.addTarget(self, action: #selector(self.answerPressed(sender:)), for: .touchUpInside)
        CardAlt.word3.addTarget(self, action: #selector(self.answerPressed(sender:)), for: .touchUpInside)
        CardAlt.word4.addTarget(self, action: #selector(self.answerPressed(sender:)), for: .touchUpInside)
        //выбираем 4 возможных ответа
        selectPossibleAnswers()
        
        //настраиваем лейбл результатов
        resultLabel.text = "Удачи!"
        resultLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        resultLabel.numberOfLines = 0
        resultLabel.textAlignment = .center
        resultLabel.center = self.view.center
        resultLabel.center.y -= 230
        
        //вручную добавляем все элементы на экран
        self.view.addSubview(resultLabel)
        self.view.addSubview(CardAlt)
        self.view.addSubview(Card1)
        self.view.addSubview(Card2)
    }
    
    //метод, проверющий ответ и вызывающий смену карточек
    @objc func answerPressed(sender: UIButton!) {
        if sender.titleLabel?.text == correctWord {
            correctCounter += 1
            swapCard(side: "rigth")
            resultLabel.text = "Правильно!\n\(correctCounter)/\(c)"
        } else {
            resultLabel.text = "Неправильно :(\n\(correctCounter)/\(c)"
            swapCard(side: "left")
        }
    }
    
    //метод, подбирающий 4 возможных ответа
    func selectPossibleAnswers() {
        let wordButtons = [CardAlt.word1, CardAlt.word2, CardAlt.word3, CardAlt.word4]
        correctWord = category.words[c].ru
        var words = [correctWord]
        for _ in 0..<3 {
            var wrongWord = category.words[Int.random(in: 0..<category.words.count)].ru
            while words.contains(wrongWord) {
                wrongWord = category.words[Int.random(in: 0..<category.words.count)].ru
            }
            words.append(wrongWord)
        }
        words.shuffle()
        for button in wordButtons {
            button.setTitle(words.popLast(), for: .normal)
        }
    }
    
    //метод, анимирующий смену карточек
    func swapCard(side: String) {
        c += 1
        //если все слова пройдены
        if c == category.words.count {
            c -= 1
            let alert = UIAlertController(title: "Тест окончен", message: "Вы знаете \(Int(100*correctCounter/(c+1)))% слов", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { result in
                self.Card1.isHidden = true
                self.Card2.isHidden = true
                self.CardAlt.isHidden = true
                self.navigationController!.popViewController(animated: true)
                return
            }))
            self.present(alert, animated: true, completion: nil)
        } else if c % 2 == 1 {
            Card2.addGestureRecognizer(gesture)
            Card2.word.text = category.words[c].en
            Card2.backgroundColor = UIColor(hex: bgColors[c % bgColors.count])
            UIView.animate(withDuration: 0.3, animations: {
                if side == "left" {
                    self.Card2.center = self.view.center
                    self.Card1.center.x = -200
                } else {
                    self.Card2.center = self.view.center
                    self.Card1.center.x += 50
                }
            }, completion: { result in
                self.Card1.center = self.lowerCenter
            })
        } else {
            Card1.addGestureRecognizer(gesture)
            Card1.word.text = category.words[c].en
            Card1.backgroundColor = UIColor(hex: bgColors[c % bgColors.count])
            UIView.animate(withDuration: 0.3, animations: {
                if side == "left" {
                    self.Card1.center = self.view.center
                    self.Card2.center.x = -200
                } else {
                    self.Card1.center = self.view.center
                    self.Card2.center.x += 50
                }
            }, completion: { result in
                self.Card2.center = self.lowerCenter
            })
        }
        UIView.animate(withDuration: 0.3, animations: {
            self.CardAlt.change(color: UIColor(hex: self.bgColors[self.c % self.bgColors.count])!)
            self.CardAlt.layer.opacity = 0
        }, completion: { result in
            self.CardAlt.layer.opacity = 1
            self.selectPossibleAnswers()
        })
        if side == "left" {
            resultLabel.text = "\(correctCounter)/\(c)"
        }
    }
    
}


extension TestViewController: UIGestureRecognizerDelegate {
    //функция для жеста перетаскивания
    @objc func wasDragged(gestureRecognizer: UIPanGestureRecognizer) {
        
        let translation = gestureRecognizer.translation(in: self.view)
        //вычисляем сдвиг относительно центра
        let offset = gestureRecognizer.view!.center.x - self.view.center.x
        CardAlt.isHidden = offset < 0 ? true : false
        
        //если жест начался, то сдвигать карточку на величину перетаскивания
        if gestureRecognizer.state == UIGestureRecognizer.State.changed {
            gestureRecognizer.view!.center.x = gestureRecognizer.view!.center.x + translation.x
            //если жест закончился, то проверяем, на сколько и в какую сторону сдвинулась карточка
            //и выполняем необходимую анимацию
        } else if gestureRecognizer.state == UIGestureRecognizer.State.ended {
            //если сдвинулась от центра меньше чем на 200, то вернуть в центр
            if abs(offset) < 200 {
                UIView.animate(withDuration: 0.15, animations: {
                    gestureRecognizer.view!.center = self.view.center
                })
            } else {
                //если влево, то поменять карточки
                if offset < 0 {
                    swapCard(side: "left")
                    //если вправо, то сдвинуть карточку почти до края
                } else {
                    UIView.animate(withDuration: 0.15, animations: {
                        gestureRecognizer.view!.center.x = CGFloat(self.view.center.x) + (offset / abs(offset)) * 300
                    })
                }
            }
        }
        gestureRecognizer.setTranslation(CGPoint(x: 0, y: 0), in: self.view)
    }
    
}

//
//  ViewController.swift
//  Dictionary App
//
//  Created by Алёна Алексиади on 10/03/2020.
//  Copyright © 2020 Алёна Алексиади. All rights reserved.
//

import UIKit
import RealmSwift


class TestCategoriesViewController: UIViewController {
    

    @IBOutlet weak var collectionView: UICollectionView!
    //цвета для ячеек
    var bgColors = ["#007EFE", "#00BB00", "#f59f00", "#FA5002", "#B40983"]
    //массив категорий
    var categories = [Category]() {
        didSet {
            updateCollectionView()
        }
    }
    private var selectedCategoryIndex = -1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionView()
        self.categories = Array(fetchAllFromRealm(type: Category.self))
    }
    
    //начальная настройка коллекции
    func setupCollectionView() {
        //назначение управляющего коллекцией и привязывание ячейки
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib.init(nibName: CategoryViewCell.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: CategoryViewCell.reuseIdentifier)
    }
    
    func updateCollectionView() {
        self.collectionView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToTestVC" {
            let vc = segue.destination as! TestViewController
            vc.category = categories[selectedCategoryIndex]
        }
    }
}

extension TestCategoriesViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryViewCell.reuseIdentifier, for: indexPath) as! CategoryViewCell
        cell.configureCell(with: categories[indexPath.row], color: bgColors[indexPath.row % bgColors.count])
        print(cell.frame)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCategoryIndex = indexPath.row
        performSegue(withIdentifier: "ToTestVC", sender: self)
    }
}

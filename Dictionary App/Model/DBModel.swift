//
//  DBModel.swift
//  Dictionary App
//
//  Created by Алёна Алексиади on 01/03/2020.
//  Copyright © 2020 Алёна Алексиади. All rights reserved.
//

import UIKit
import RealmSwift

class Category: Object {
    @objc dynamic var name_ru = ""
    @objc dynamic var name_en = ""
    @objc dynamic var version = 0.0
    @objc dynamic var icon = ""
    let words = LinkingObjects(fromType: Word.self, property: "category")
    
    override static func primaryKey() -> String? {
        return "name_en"
    }
}

class Word: Object {
    @objc dynamic var en = ""
    @objc dynamic var ru = ""
    @objc dynamic var category: Category?
}

func saveCategoryToRealm(category: CategoryResponse) {
    let realm = try! Realm()

    let newCategory = Category()
    newCategory.name_en = category.name_en
    newCategory.name_ru = category.name_ru
    newCategory.version = category.version
    newCategory.icon = category.icon
    
    do {
        try realm.write() {
            realm.add(newCategory, update: .modified)
        }
    } catch let error {
        print(error)
    }
}

func saveWordsToRealm(words: [WordResponse], of category: String) {
    let realm = try! Realm()
    let category = realm.objects(Category.self).filter("name_en == '\(category)'").first!
    var newWords = [Word]()
    for w in words {
        let word = Word()
        word.en = w.en
        word.ru = w.ru
        word.category = category
        newWords.append(word)
    }
    do {
        try realm.write() {
            realm.delete(category.words)
            realm.add(newWords)
        }
    } catch let error {
        print(error)
    }
}

func fetchAllFromRealm<T:Object>(type: T.Type) -> Results<T> {
    let realm = try! Realm()
    let results = realm.objects(T.self)
    return results
}

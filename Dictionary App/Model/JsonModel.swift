//
//  JsonModel.swift
//  Dictionary App
//
//  Created by Алёна Алексиади on 01/03/2020.
//  Copyright © 2020 Алёна Алексиади. All rights reserved.
//

import UIKit

struct CategoryResponse: Codable {
    var name_ru = ""
    var name_en = ""
    var version = 0.0
    var icon = ""
    var words = ""
}

struct WordResponse: Codable {
    var en = ""
    var ru = ""
}

func loadJson<T:Codable>(of type: T.Type, from url: String, result: @escaping ([T]) -> Void) {
    if let url = URL(string: url) {
       URLSession.shared.dataTask(with: url) { data, response, error in
          if let data = data {
              do {
                 let json = try JSONDecoder().decode([T].self, from: data)
                 result(json)
              } catch let error {
                 print(error)
              }
           }
       }.resume()
    }
}

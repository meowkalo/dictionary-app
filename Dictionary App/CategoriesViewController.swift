//
//  ViewController.swift
//  Dictionary App
//
//  Created by Алёна Алексиади on 29/02/2020.
//  Copyright © 2020 Алёна Алексиади. All rights reserved.
//

import UIKit
import RealmSwift


class CategoriesViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    //цвета для ячеек
    var bgColors = ["#007EFE", "#00BB00", "#f59f00", "#FA5002", "#B40983"]
    //массив категорий
    var categories = [Category]() {
        didSet {
            updateCollectionView()
        }
    }
    private var selectedCategoryIndex = -1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionView()
        loadCategories() {
            DispatchQueue.main.async {
                self.categories = Array(fetchAllFromRealm(type: Category.self))
            }
        }
    }
    
    //начальная настройка коллекции
    func setupCollectionView() {
        //назначение управляющего коллекцией и привязывание ячейки
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib.init(nibName: CategoryViewCell.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: CategoryViewCell.reuseIdentifier)
    }
    
    func updateCollectionView() {
        self.collectionView.reloadData()
    }
    
    func loadCategories(success: @escaping () -> Void) {
        categories = Array(fetchAllFromRealm(type: Category.self))
        loadJson(of: CategoryResponse.self, from: "https://api.myjson.com/bins/nlh2q") { categories in
            for c in categories {
                if let version = defaults.value(forKey: c.name_en) as? Double {
                    if version < c.version {
                        defaults.set(c.version, forKey: c.name_en)
                        saveCategoryToRealm(category: c)
                        loadJson(of: WordResponse.self, from: c.words) { words in
                            saveWordsToRealm(words: words, of: c.name_en)
                        }
                    }
                } else {
                    defaults.set(c.version, forKey: c.name_en)
                    saveCategoryToRealm(category: c)
                    loadJson(of: WordResponse.self, from: c.words) { words in
                        saveWordsToRealm(words: words, of: c.name_en)
                    }
                }
            }
            success()
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToCategoryWordsVC" {
            let vc = segue.destination as! CategoryWordsViewController
            vc.category = categories[selectedCategoryIndex]
        }
    }
}

extension CategoriesViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryViewCell.reuseIdentifier, for: indexPath) as! CategoryViewCell
        cell.configureCell(with: categories[indexPath.row], color: bgColors[indexPath.row % bgColors.count])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCategoryIndex = indexPath.row
        performSegue(withIdentifier: "ToCategoryWordsVC", sender: self)
    }
}

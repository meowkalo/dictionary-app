//
//  CategoryWordsViewController.swift
//  Dictionary App
//
//  Created by Алёна Алексиади on 03/03/2020.
//  Copyright © 2020 Алёна Алексиади. All rights reserved.
//

import UIKit

class CategoryWordsViewController: UITableViewController {
    
    var category = Category()
    var words = [Word]()
    let search = UISearchController(searchResultsController: nil)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = category.name_ru
        words = Array(category.words)
        tableView.reloadData()
        search.searchResultsUpdater = self
        search.obscuresBackgroundDuringPresentation = false
        search.searchBar.placeholder = "Поиск"
        navigationItem.searchController = search
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return words.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WordCell", for: indexPath)
        cell.textLabel?.text = words[indexPath.row].ru
        cell.detailTextLabel?.text = words[indexPath.row].en
        return cell
    }
}

extension CategoryWordsViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let filter = searchController.searchBar.text!.lowercased()
        if filter == "" {
            words = Array(category.words)
        } else {
            words = Array(category.words).filter({$0.en.lowercased().contains(filter) || $0.ru.lowercased().contains(filter)})
        }
        tableView.reloadData()
    }
}

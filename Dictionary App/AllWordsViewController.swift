//
//  AllWordsViewController.swift
//  Dictionary App
//
//  Created by Алёна Алексиади on 08/03/2020.
//  Copyright © 2020 Алёна Алексиади. All rights reserved.
//

import UIKit

class AllWordsViewController: UITableViewController {
    
    let search = UISearchController(searchResultsController: nil)
    var bgColors = ["#007EFE", "#00BB00", "#f59f00", "#FA5002", "#B40983"]
    var categories = [(name: String, words: [Word])]()
    var filteredCategories = [(name: String, words: [Word])]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        search.searchResultsUpdater = self
        search.obscuresBackgroundDuringPresentation = false
        search.searchBar.placeholder = "Поиск"
        navigationItem.searchController = search
        
        let queueResult = fetchAllFromRealm(type: Category.self)
        for i in queueResult {
            categories.append((name: i.name_ru, words: Array(i.words)))
        }
        filteredCategories = categories
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return filteredCategories[section].name
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return filteredCategories.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredCategories[section].words.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WordCell", for: indexPath)
        cell.textLabel?.text = filteredCategories[indexPath.section].words[indexPath.row].ru
        cell.detailTextLabel?.text = filteredCategories[indexPath.section].words[indexPath.row].en
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor(hex: bgColors[section % bgColors.count])
        header.textLabel?.textColor = .white
        
    }
    
}

extension AllWordsViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let filter = searchController.searchBar.text!.lowercased()
        if filter == "" {
            filteredCategories = categories
        } else {
            filteredCategories = []
            for c in categories {
                let words = c.words.filter({$0.en.lowercased().contains(filter) || $0.ru.lowercased().contains(filter)})
                if words.count > 0 {
                    filteredCategories.append((name: c.name, words: words))
                }
            }
        }
        tableView.reloadData()
    }
}
